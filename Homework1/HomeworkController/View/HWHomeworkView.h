//
//  HWHomeworkView.h
//  Homework1
//
//  Created by Vladislav Grigoriev on 20/09/16.
//  Copyright © 2016 com.inostudio.ios.courses. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HWHomeworkViewDelegate;

@interface HWHomeworkView : UIView

@property (nonatomic, strong, readonly) UIScrollView *scrollView;

@property (nonatomic, copy, readonly) NSArray<UIView *> *views;
@property (nonatomic, assign) UIEdgeInsets contentInsets;

@property (nonatomic, strong) id<HWHomeworkViewDelegate> delegate;

- (void)addView:(UIView *)view;

@end

@protocol HWHomeworkViewDelegate <NSObject>

@required
- (void)homeworkViewShouldEndEditing:(HWHomeworkView *)view;

@end
