//
//  UIView+HWShadow.m
//  HelpWill
//
//  Created by Viktor Peschenkov on 06.07.16.
//  Copyright © 2016 INOSTUDIO. All rights reserved.
//

@implementation UIView (HWShadow)

- (void)hw_setShadow {
    [self hw_setShadow:[UIColor hw_colorWithHexInt:HWCalypsoColorHex]
                offset:CGSizeMake(0.0f, 2.5f)
               opacity:0.40f
                radius:3.0f];
}

- (void)hw_setShadow:(UIColor *)color offset:(CGSize)offset opacity:(CGFloat)opacity radius:(CGFloat)radius {
    self.layer.shadowColor = color.CGColor;
    self.layer.shadowOffset = offset;
    self.layer.shadowOpacity = opacity;
    self.layer.shadowRadius = radius;
    self.layer.masksToBounds = NO;
}

- (void)hw_removeShadow {
    self.layer.shadowColor = [UIColor clearColor].CGColor;
}

@end
